# ivl-deploy

```bash
$ git clone --recursive git@gitlab.com:pokeguy/ivl-deploy.git
$ git submodule update --recursive --remote
$ docker-compose up -d
```